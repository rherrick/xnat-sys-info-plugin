# XNAT 1.7 System Info Plugin #

This is the XNAT 1.7 System Info Plugin. It provides information about the XNAT deployment in
which it is installed, including:

* Run-time environment properties

# Building #

To build the XNAT 1.7 system info plugin:

1. If you haven't already, clone this repository and cd to the newly cloned folder.
1. Build the plugin: `./gradlew jar` (on Windows, you can use the batch file: `gradlew.bat jar`). This should build 
   the plugin in the file **build/libs/xnat-sys-info-plugin-_version_.jar** (the version may differ based on updates
   to the code).
1. Copy the plugin jar to your plugins folder: `cp build/libs/xnat-sys-info-plugin-1.0.0.jar /data/xnat/home/plugins`

# Using #

Currently this plugin only provides REST services to retrieve system information. The supported REST calls include:

## Environment ##

The **/xapi/system/environment** call provides a list of the properties available in the XNAT run-time environment.
These are returned as a JSON map of key-value pairs.
