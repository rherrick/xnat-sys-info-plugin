/*
 * xnat-sys-info-plugin: org.nrg.xnat.plugins.sysinfo.rest.TemplateApi
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.plugins.sysinfo.rest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.nrg.framework.annotations.XapiRestController;
import org.nrg.xapi.rest.AbstractXapiRestController;
import org.nrg.xapi.rest.XapiRequestMapping;
import org.nrg.xdat.security.services.RoleHolder;
import org.nrg.xdat.security.services.UserManagementServiceI;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.AbstractEnvironment;
import org.springframework.core.env.Environment;
import org.springframework.core.env.MapPropertySource;
import org.springframework.core.env.PropertySource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.support.ServletContextPropertySource;

import java.util.Map;
import java.util.TreeMap;

@Api(description = "XNAT 1.7 System Info API")
@XapiRestController
@RequestMapping(value = "/sysinfo")
public class SysInfoApi extends AbstractXapiRestController {
    @Autowired
    protected SysInfoApi(final UserManagementServiceI userManagementService, final RoleHolder roleHolder, final Environment environment) {
        super(userManagementService, roleHolder);
        _environment = environment;
    }

    @ApiOperation(value = "Returns a list of the properties configured in the XNAT system environment.", response = String.class, responseContainer = "Map")
    @ApiResponses({@ApiResponse(code = 200, message = "Environment variables successfully retrieved."),
                   @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
                   @ApiResponse(code = 403, message = "Not authorized to view the system environment."),
                   @ApiResponse(code = 500, message = "An unexpected error occurred.")})
    @XapiRequestMapping(value = "environment", produces = {MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.GET)
    public ResponseEntity<Map<String, Object>> getEntities() {
        final HttpStatus status = isPermitted();
        if (status != null) {
            LoggerFactory.getLogger(SysInfoApi.class).warn("Unauthorized access attempt by user {}", getSessionUser().getUsername());
            return new ResponseEntity<>(status);
        }
        final Map<String, Object> map = new TreeMap<>();
        for (final PropertySource<?> propertySource : ((AbstractEnvironment) _environment).getPropertySources()) {
            if (propertySource instanceof MapPropertySource) {
                final Map<String, Object> source = ((MapPropertySource) propertySource).getSource();
                for (final String key : source.keySet()) {
                    final Object value = source.get(key);
                    if (value == null) {
                        map.put(key, "(null)");
                        continue;
                    }
                    final String className = value.getClass().getName();
                    if (className.matches("java\\.lang\\.[A-Z][A-z]+")) {
                        map.put(key, value);
                        continue;
                    }
                    map.put(key, value.toString() + " (" + className + ")");
                }
            } else if (propertySource instanceof ServletContextPropertySource) {
                final String[] names = ((ServletContextPropertySource) propertySource).getPropertyNames();
                if (names != null) {
                    for (final String name : names) {
                        map.put(name, ((ServletContextPropertySource) propertySource).getProperty(name));
                    }
                }
            }
        }

        return new ResponseEntity<>(map, HttpStatus.OK);
    }

    private final Environment _environment;
}
