/*
 * xnat-sys-info-plugin: org.nrg.xnat.plugins.sysinfo.plugin.XnatTemplatePlugin
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.plugins.sysinfo.plugin;

import org.nrg.framework.annotations.XnatPlugin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.ComponentScan;

@XnatPlugin(value = "xnatSysInfoPlugin", name = "XNAT 1.7 System Info Plugin")
@ComponentScan({"org.nrg.xnat.plugins.sysinfo.rest"})
public class SysInfoPlugin {
    public SysInfoPlugin() {
        _log.info("Creating the SysInfoPlugin configuration class");
    }

    private static final Logger _log = LoggerFactory.getLogger(SysInfoPlugin.class);
}
